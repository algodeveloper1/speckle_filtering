# -*- coding: utf-8 -*-


##### Example of computation in Slant Range geometry
from osgeo import gdal
from gdalconst import GA_ReadOnly
import numpy as np
import scipy.signal as sg
import os, os.path, optparse, sys

def boxcarFilter(image, size=5):
    'Apply a boxcar filter on a given image (basic multilooking)'

    
    # If the window is square:
    if isinstance(size, int):
        size = [size, size]
    
    # Basic boxcar filter:
    return sg.convolve2d(image, np.ones(size), 'same') / (size[0] * size[1])

def prog2(inputFilename,size,outputFilename):
    print('Open the file')
    # Open input image in slant range geometry:
    if os.path.exists(inputFilename):
        input_image_driver = gdal.Open(inputFilename, GA_ReadOnly)
        input_image = input_image_driver.ReadAsArray()
        print('Apply Boxcar filter')
        output_image = boxcarFilter(input_image)
    
        # Conversion in dB:
        #output_image = 10 * np.log10(output_image)

    
        ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
        # Save output image in slant range geometry:
        outdriver = gdal.GetDriverByName('GTiff')
        output_image_driver = outdriver.Create(outputFilename, input_image_driver.RasterXSize, input_image_driver.RasterYSize, 1, gdal.GDT_Float32)
        output_image_driver.GetRasterBand(1).WriteArray(output_image)
    
        # Close data sets:
        input_image_driver = None
        output_image_driver = None
        input_image=None
        output_image=None

          
    else : 
        print('The input file does not exist in the directory')
    # Example of basic multilooking (5x5):
    
    
if __name__ == '__main__':
 

    from properties.p import Property
    import quicklook_raster
    if os.path.isfile('/projects/speckle_filtering/conf/configuration.properties'):
            
        configfile='/projects/speckle_filtering/conf/configuration.properties'
        print('The processing is launched using ', configfile)
        prop=Property()
        prop_dict=prop.load_property_files(configfile)
    
        prog2(prop_dict['inputfile'], prop_dict['size'],prop_dict['outputfile'])
        quicklook_raster.main(prop_dict['outputfile'])
    


